# Legend of the Five Rings (5th Edition) authorized by [Edge Studio](https://edge-studio.net/)

![Banner Legend of the Five Rings](./l5rBan.jpg)
[![Buy Me a Coffee](./tags-bymecoffee.svg)](https://ko-fi.com/vlyan)
[![FoundryVTT version](https://img.shields.io/badge/FVTT-v10.x-informational)](https://foundryvtt.com/)
[![Forge Installs](https://img.shields.io/badge/dynamic/json?label=Forge%20Installs&query=package.installs&suffix=%25&url=https%3A%2F%2Fforge-vtt.com%2Fapi%2Fbazaar%2Fpackage%2Fl5r5e&colorB=4aa94a)](https://forge-vtt.com/bazaar#package=l5r5e)
[![Foundry Hub Endorsements](https://img.shields.io/endpoint?logoColor=white&url=https%3A%2F%2Fwww.foundryvtt-hub.com%2Fwp-json%2Fhubapi%2Fv1%2Fpackage%2Fl5r5e%2Fshield%2Fendorsements)](https://www.foundryvtt-hub.com/package/l5r5e/)
[![Foundry Hub Comments](https://img.shields.io/endpoint?logoColor=white&url=https%3A%2F%2Fwww.foundryvtt-hub.com%2Fwp-json%2Fhubapi%2Fv1%2Fpackage%2Fl5r5e%2Fshield%2Fcomments)](https://www.foundryvtt-hub.com/package/l5r5e/)

# English
This is a game system for [Foundry Virtual Tabletop](https://foundryvtt.com/) provides a character sheet and basic systems to play [Edge Studio's](https://edge-studio.net/) Legend of the Five Rings 5th edition.
This version is authorized by Edge Studio, all texts, images and copyrights are the property of their respective owners.


## Installation & Recommended modules
See the [Wiki page - Installation](https://gitlab.com/teaml5r/l5r5e/-/wikis/users/install.md) for detail.


## Current L5R team (alphabetical order)
- Carter (compendiums, adventure adaptation)
- Vlyan (development)


## Historical L5R team (alphabetical order)
- Carter (compendiums, adventure adaptation)
- Hrunh (compendiums, pre-gen characters adaptation)
- Mandar (development)
- Sasmira (contributor)
- Vlyan (development)


## Acknowledgements, vielen danke & Many thanks to :
1. José Ladislao Lainez Ortega, aka "L4D15", for his first version.
2. Sasmira and LeRatierBretonnien for their work on the [Francophone community of Foundry](https://discord.gg/pPSDNJk).
3. Flex for advices in L5R rules.


## Contribute
You are free to contribute and propose corrections, modifications after fork.

See the [Wiki page - System helping (Contribute)](https://gitlab.com/teaml5r/l5r5e/-/wikis/dev/system-helping.md) for detail.



# French
Il s'agit d'un système de jeu pour [Foundry Virtual Tabletop](https://foundryvtt.com/) qui fournit une fiche de personnage et des systèmes de base pour jouer à La Légende des Cinq Anneaux 5e édition de [Edge Studio](https://edge-studio.net/).
Cette version est autorisée par Edge Studio, tous les textes, images et droits d'auteur reviennent à leurs propriétaires respectifs.


## Installation & Modules recommandés
Le wiki en français est disponible à cette adresse :
> https://foundryvtt.wiki/fr/systemes/l5r

La liste des modules recommandés mise à jour est aussi disponible sur le [Wiki - Installation](https://gitlab.com/teaml5r/l5r5e/-/wikis/users/install.md#modules) (en anglais).


## Nous rejoindre
1. Vous pouvez retrouver toutes les mises à jour en français pour FoundryVTT sur le discord officiel francophone
2. Lien vers [Discord Francophone](https://discord.gg/pPSDNJk)


## L'équipe L5R actuelle (par ordre alphabétique)
- Carter (compendiums, adaptation de scénario)
- Vlyan (développement)


## L'équipe L5R historique (par ordre alphabétique)
- Carter (compendiums, adaptation de scénario)
- Hrunh (compendiums, adaptation des pré-tirés)
- Mandar (développement)
- Sasmira (contributeur)
- Vlyan (développement)


## Remerciements, vielen danke & Many thanks to :
1. José Ladislao Lainez Ortega, aka "L4D15", pour sa première version.
2. Sasmira et LeRatierBretonnien pour leur travail sur la [communauté Francophone de Foundry](https://discord.gg/pPSDNJk).
3. Flex pour ses conseils sur les règles de L5R.


## Contribuer
Vous êtes libre de contribuer et proposer après fork des corrections, modifications.

Voir la [page du Wiki - System helping (Contribute)](https://gitlab.com/teaml5r/l5r5e/-/wikis/dev/system-helping.md) pour le détail (en anglais).



# Screenshots
![L5R 5e FoundryVTT Connection](./screenshoots/login.jpg?raw=true)
![L5R 5e FoundryVTT Roll n Keep](./screenshoots/roll.jpg?raw=true)
![L5R 5e FoundryVTT School](./screenshoots/school.jpg?raw=true)
![L5R 5e FoundryVTT Pc](./screenshoots/sheet_pc.jpg?raw=true)
![L5R 5e FoundryVTT Npc](./screenshoots/sheet_npc_army.jpg?raw=true)
![L5R 5e FoundryVTT Compendiums](./screenshoots/compendiums.jpg?raw=true)
