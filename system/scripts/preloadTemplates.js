export const PreloadTemplates = async function () {
    const tpl = CONFIG.l5r5e.paths.templates;
    return loadTemplates([
        // Add paths to "systems/l5r5e/templates"
        // *** Actors : PC ***
        `${tpl}actors/character/advancement-school.html`,
        `${tpl}actors/character/advancement-others.html`,
        `${tpl}actors/character/attributes.html`,
        `${tpl}actors/character/category.html`,
        `${tpl}actors/character/conflict.html`,
        `${tpl}actors/character/experience.html`,
        `${tpl}actors/character/identity.html`,
        `${tpl}actors/character/inventory.html`,
        `${tpl}actors/character/narrative.html`,
        `${tpl}actors/character/rings.html`,
        `${tpl}actors/character/skill.html`,
        `${tpl}actors/character/social.html`,
        `${tpl}actors/character/stance.html`,
        `${tpl}actors/character/techniques.html`,
        `${tpl}actors/character/twenty-questions-item.html`,
        // *** Actors : Npc ***
        `${tpl}actors/npc/attributes.html`,
        `${tpl}actors/npc/conflict.html`,
        `${tpl}actors/npc/identity.html`,
        `${tpl}actors/npc/inventory.html`,
        `${tpl}actors/npc/narrative.html`,
        `${tpl}actors/npc/rings.html`,
        `${tpl}actors/npc/social.html`,
        `${tpl}actors/npc/skill.html`,
        `${tpl}actors/npc/techniques.html`,
        // *** Actors : Army ***
        `${tpl}actors/army/army.html`,
        `${tpl}actors/army/cohort.html`,
        `${tpl}actors/army/fortification.html`,
        `${tpl}actors/army/others.html`,
        // *** Items ***
        `${tpl}items/advancement/advancement-entry.html`,
        `${tpl}items/advancement/advancement-sheet.html`,
        `${tpl}items/armor/armors.html`,
        `${tpl}items/armor/armor-entry.html`,
        `${tpl}items/armor/armor-sheet.html`,
        `${tpl}items/bond/bond-entry.html`,
        `${tpl}items/bond/bond-sheet.html`,
        `${tpl}items/bond/bond-text.html`,
        `${tpl}items/item/items.html`,
        `${tpl}items/item/item-entry.html`,
        `${tpl}items/item/item-value.html`,
        `${tpl}items/item/item-sheet.html`,
        `${tpl}items/item/item-infos.html`,
        `${tpl}items/item/item-text.html`,
        `${tpl}items/item-pattern/item-pattern-entry.html`,
        `${tpl}items/item-pattern/item-pattern-sheet.html`,
        `${tpl}items/item-pattern/item-pattern-text.html`,
        `${tpl}items/peculiarity/peculiarity-entry.html`,
        `${tpl}items/peculiarity/peculiarity-sheet.html`,
        `${tpl}items/peculiarity/peculiarity-text.html`,
        `${tpl}items/property/properties.html`,
        `${tpl}items/property/property-entry.html`,
        `${tpl}items/property/property-sheet.html`,
        `${tpl}items/signature-scroll/signature-scroll-entry.html`,
        `${tpl}items/signature-scroll/signature-scroll-sheet.html`,
        `${tpl}items/signature-scroll/signature-scroll-text.html`,
        `${tpl}items/technique/technique-entry.html`,
        `${tpl}items/technique/technique-sheet.html`,
        `${tpl}items/technique/technique-text.html`,
        `${tpl}items/title/title-entry.html`,
        `${tpl}items/title/title-sheet.html`,
        `${tpl}items/title/title-text.html`,
        `${tpl}items/weapon/weapons.html`,
        `${tpl}items/weapon/weapon-entry.html`,
        `${tpl}items/weapon/weapon-sheet.html`,
        `${tpl}items/army-cohort/army-cohort-entry.html`,
        `${tpl}items/army-fortification/army-fortification-entry.html`,
    ]);
};
